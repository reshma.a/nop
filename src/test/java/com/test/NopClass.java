package com.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NopClass {
	static WebDriver driver;

	@BeforeClass
	private void launchApplication() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://admin-demo.nopcommerce.com/login");
	}

	@Test(priority = 0)
	public void Login() throws InterruptedException {
		driver.findElement(By.className("email")).clear();
		driver.findElement(By.className("email")).sendKeys("admin@yourstore.com");
		driver.findElement(By.className("password")).sendKeys("admin");
		driver.findElement(By.id("RememberMe")).click();
		driver.findElement(By.xpath("//button[text()='Log in']")).click();
		driver.findElement(By.xpath("//button[text()='Log in']")).click();
		WebElement john = driver.findElement(By.linkText("John Smith"));
		String text = john.getText();
		Assert.assertEquals(text, "John Smith");
	}

	@Test(priority = 1, enabled = true)
	public void categories() throws InterruptedException, IOException {
		driver.findElement(By.partialLinkText("Catalog")).click();
		Thread.sleep(2000);
		driver.findElement(By.partialLinkText("Categories")).click();
		driver.findElement(By.partialLinkText("Add new")).click();
		File f = new File("/home/reshma/Documents/datadriven/nop.xlsx");
		FileInputStream files = new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(files);
		XSSFSheet Sheet1 = workbook.getSheetAt(0);
		int row = Sheet1.getPhysicalNumberOfRows();

		for (int i = 0; i < row; i++) {
			String string1 = Sheet1.getRow(i).getCell(0).getStringCellValue();
			String description = Sheet1.getRow(i).getCell(1).getStringCellValue();
			String lenovo = Sheet1.getRow(i).getCell(2).getStringCellValue();

			driver.findElement(By.name("Name")).sendKeys(string1);
			Thread.sleep(3000);
			driver.switchTo().frame(0);
			driver.findElement(By.id("tinymce")).sendKeys(description);
			driver.switchTo().defaultContent();
			Thread.sleep(3000);
			WebElement parent = driver.findElement(By.id("ParentCategoryId"));
			Select sel = new Select(parent);
			sel.selectByIndex(1);
			driver.findElement(By.name("save")).click();
			driver.findElement(By.xpath("//div[text()='Search']"));
			driver.findElement(By.name("SearchCategoryName")).sendKeys(string1);
			driver.findElement(By.id("search-categories")).click();
			Thread.sleep(2000);
			String computer = driver.findElement(By.xpath("(//td[text()='Computers >> Lenovo'])[1]")).getText();
			Assert.assertEquals(computer, lenovo);
		}
	}

	@Test(priority = 2, enabled = true)
	private void product() {
		driver.findElement(By.xpath("//p[text()=' Products']")).click();
		driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
		WebElement category = driver.findElement(By.id("SearchCategoryId"));
		Select sl = new Select(category);
		sl.selectByValue("2");
		driver.findElement(By.id("search-products")).click();
		String text = driver.findElement(By.xpath("//td[text()='Build your own computer']")).getText();
		Assert.assertEquals(text, "Build your own computer");
	}

	@Test(priority = 3)
	public void manufacturer() throws InterruptedException, IOException {
		driver.findElement(By.partialLinkText("Manufacturers")).click();
		driver.findElement(By.partialLinkText("Add new")).click();
		File f = new File("/home/reshma/Documents/datadriven/nop.xlsx");
		FileInputStream files = new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(files);
		XSSFSheet Sheet1 = workbook.getSheetAt(1);
		int row = Sheet1.getPhysicalNumberOfRows();
		for (int i = 0; i < row; i++) {
			String name = Sheet1.getRow(i).getCell(0).getStringCellValue();
			String pass = Sheet1.getRow(i).getCell(1).getStringCellValue();
			driver.findElement(By.id("Name")).sendKeys(name);
			driver.switchTo().frame(0);
			driver.findElement(By.id("tinymce")).sendKeys(pass);
			driver.switchTo().defaultContent();
			Thread.sleep(2000);
			WebElement advanced = driver.findElement(By.xpath("//label[@class='onoffswitch-label']"));

			if (advanced.isEnabled()) {
				WebElement USD = driver.findElement(By.xpath("//input[@title='0.0000 USD']"));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView(true)", USD);
			} else {
				driver.findElement(By.xpath("//label[@class='onoffswitch-label']")).click();
				driver.findElement(By.id("manufacturer-display")).click();
				WebElement USD = driver.findElement(By.xpath("//input[@title='0.0000 USD']"));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView(true)", USD);
			}

			driver.findElement(By.xpath("//input[@title=\"0.0000 USD\"]")).sendKeys("50");
			driver.findElement(By.xpath("//input[@title=\"10000.0000 USD\"]")).sendKeys("60");
			driver.findElement(By.xpath("//input[@title=\"0 \"]")).sendKeys("1");
			driver.findElement(By.name("save")).click();
			driver.findElement(By.name("SearchManufacturerName")).sendKeys(name);
			driver.findElement(By.id("search-manufacturers")).click();
		

		String text = driver.findElement(By.xpath("(//td[text()='Samsung'])[1]")).getText();
		Assert.assertNotSame(text, name);
	}}

	@Test(priority = 4, enabled = true)
	public void logout() {
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
	}

	@AfterClass
	private void closeApplication() {
		driver.quit();
	}
}
